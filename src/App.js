import React, { Component } from 'react';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Button from '@material-ui/core/Button';
import Paper from '@material-ui/core/Paper';
import MenuItem from '@material-ui/core/MenuItem';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
import Grid from '@material-ui/core/Grid';
import 'typeface-roboto';
import Typography from '@material-ui/core/Typography';
import scrollToComponent from 'react-scroll-to-component';
import Chip from '@material-ui/core/Chip';
import AceEditor from 'react-ace'
import axios from 'axios'
import 'brace/mode/java';
import 'brace/theme/dracula';
import './App.css';
import { createMuiTheme, MuiThemeProvider } from '@material-ui/core/styles';
import SimpleExpansionPanel from './SimpleExpansionPanel'
import TestCases from './TestCases'
import Switch from '@material-ui/core/Switch';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#44475a',
    },
    secondary: {
      main: '#aaaaaa',
    },
    error: {
      main: '#ffffff',
    }
  },
  typography: {
    useNextVariants: true,
  },
});

const buttonStyle = {
  width: "100%",
  height: "50px",
};

const amarginv = {
  marginTop: "45px",
  marginBottom: "35px"
}

var listItems = [];
var correctAns = 0;
var totalProb = 0;
var compileErrorMessage = ""
var isCompileError = false;
var nyanMessage = ""

class App extends Component {


  constructor(props) {
    super(props)
    var test_name = this.props.match.params.test_name
    if (test_name == null || test_name === "") {
      test_name = 'TP4'
    }
    this.state = {
      value: "",
      nyan: false,
      labelWidth: 0,
      age: test_name,
      result: [],
      showResult: false,
      showError: false,
      showCases: true,
      isDebug: false,
    }
    this.onChange = this.onChange.bind(this)
    this.kirim = this.kirim.bind(this)
  }

  onChange(newValue) {
    // this.state.value = newValue;
    this.setState({ value: newValue })
  }


  async kirim() {
    if (this.state.value === null) {
      return
    }

    nyanMessage = "grading..."
    this.setState({
      nyan: true,
      showResult: false,
      showError: false,
      showCases: false,
    })
    scrollToComponent(this.refs.nani)
    var url = 'https://aren-api.herokuapp.com/grader/'
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
      url = 'http://localhost:8080/grader/'
      // url = 'http://aren-api.herokuapp.com/grader/'
    }
    await axios.post(url, {
      code: this.state.value,
      name: this.state.age,
    }).then(res => {
      isCompileError = res.data["isCompileError"];
      compileErrorMessage = res.data["compileErrorMessage"];
      const results = res.data["caseResults"];
      totalProb = results.length
      correctAns = 0
      listItems = results.map((result, index) => {

        var realpass = false;

        if (this.state.isDebug) {
          const answerlen = result.expected.length;
          const realAnswer = result.answer.slice(-1 * answerlen)
          const realTrue = () => {
            console.log()
            console.log("realAns")
            console.log(realAnswer)
            console.log("expected")
            console.log(result.expected)
            if (realAnswer === result.expected) {
              return true
            } else {
              return false
            }
          }
          realpass = realTrue()
        } else {
          realpass = result.pass
        }
        console.log("aa")
        console.log(realpass)

        // console.log(index + " " + result.expected.length)
        let props = {
          duration: result.duration / 1000000,
          answer: result.answer,
          expected: result.expected,
          problem: result.problem,
          timeout: result.timeout,
          pass: realpass,
          number: index,
          debug: this.state.isDebug,
          contributor: result.contributor,
        }

        if (realpass) {
          correctAns++
          console.log(correctAns)
        }
        if (result.timeout) {
          return (
            <div>
              <Paper className="paperuu">
                <Typography variant="h5" component="h3">
                  #{index + 1}
                  <Chip label="Time Limit Exceed" style={{ backgroundColor: '#ff5555', color: '#f8f8f2' }} className="chip" />
                </Typography>
              </Paper>
            </div>
          )
        }

        return (
          <SimpleExpansionPanel {...props} key={index + 1} />
        )
      }

      );

      this.setState({
        nyan: false,
        showResult: true,
      })
    }).catch(err => {
      console.log(err)
      this.setState({
        nyan: false,
        showError: true,
      })
    });
  }

  handleDebugChange = name => event => {
    this.setState({ [name]: event.target.checked });
    this.setState({ isDebug: event.target.checked })
  };

  handleChange = async event => {
    this.setState({
      [event.target.name]: event.target.value,
      age: event.target.value
    });
    this.setState({
      // nyan: true,
      showCases: false,
    })
    console.log("test change")
    var resultsa = JSON.parse(localStorage.getItem(event.target.value))
    console.log("resultsa")
    console.log(resultsa)
    listItems = []
    if (resultsa != null) {
      listItems = resultsa.map((result, index) => {
        let props = {
          answer: result.answer.replace(/\\n/g, "\n"),
          problem: result.problem.replace(/\\n/g, "\n"),
          number: index,
          contributor: result.contributor,
        }
        return (
          <TestCases {...props} key={index + 1} />
        )
      }

      );
      console.log("showcase")
      this.setState({
        // nyan: false,
        showCases: true,
      })
      console.log("showcase")

      this.setState({
        // nyan: false,
        showCases: true,
      })
    }


    await this.loadTestCase(event.target.value)
  };

  componentDidMount() {
    var exit_url = 'http://localhost:2121/exit'
    window.addEventListener('beforeunload',()=>{axios.get(exit_url)})
    var test_name = this.props.match.params.test_name
    if (test_name != null && test_name !== "") {
      this.setState({ age: test_name })
    }
    var resultsa = JSON.parse(localStorage.getItem(this.state.age))
    console.log("resultsa")
    console.log(resultsa)
    if (resultsa != null) {
      listItems = resultsa.map((result, index) => {
        let props = {
          answer: result.answer.replace(/\\n/g, "\n"),
          problem: result.problem.replace(/\\n/g, "\n"),
          number: index,
          contributor: result.contributor,
        }
        return (
          <TestCases {...props} key={index + 1} />
        )
      }

      );
      console.log("showcase")
      this.setState({
        //   nyan: false,
        showCases: true,
      })
      console.log("showcase")
    }

    this.loadTestCase(this.state.age)
  }

  async loadTestCase(nani) {
    nyanMessage = 'loading testcase...'
    // this.setState({
    //   showCases: false,
    //   showError: false,
    //   showResult: false,
    //   // nyan: true,
    // })
    console.log("aabccd")
    console.log("aabccda")

    var url = 'https://aren-api.herokuapp.com/case/'
    if (!process.env.NODE_ENV || process.env.NODE_ENV === 'development') {
      url = 'http://localhost:8080/case/'
      // url = 'http://aren-api.herokuapp.com/grader/'
    }
    console.log(this.state.age)
    console.log(nani)
    axios.post(url, {
      testName: nani,
    }).then(res => {
      const results = res.data["cases"];
      console.log(results)
      console.log("yaa")
      var a = JSON.stringify(results)
      if (localStorage.getItem(this.state.age) !== a) {
        localStorage.setItem(this.state.age, a)
        listItems = results.map((result, index) => {
          let props = {
            answer: result.answer.replace(/\\n/g, "\n"),
            problem: result.problem.replace(/\\n/g, "\n"),
            number: index,
            contributor: result.contributor,
          }
          return (
            <TestCases {...props} key={index + 1} />
          )
        }

        );

        this.setState({
          nyan: false,
          showCases: true,
        })
      }
    }).catch(err => {
      console.log("error")
      // this.setState({
      //   nyan: false,
      //   showError: true,
      // })
    });
  }




  render() {
    return (
      <MuiThemeProvider theme={theme}>
        <div className="App">
          <Typography variant="h2" gutterBottom style={amarginv}>
            AREN 2.1
      </Typography>
          <div className="some">
            <Paper elevation={5} className="editor">
              <AceEditor
                showPrintMargin={false}
                mode="java"
                theme="dracula"
                name="editor"
                fontSize={17}
                onChange={this.onChange}
                editorProps={{ $blockScrolling: true }}
                width="795px"
                height="600px"
                value={this.state.value}
                highlightActiveLine={false}
              />
            </Paper>
          </div>

          {/* <Grid container spacing={24} style={aHeight}> */}
          <Grid container spacing={24}>
            <Grid item xs={5}>
              <FormControl style={buttonStyle}>
                <Select
                  value={this.state.age}
                  onChange={this.handleChange}
                  displayEmpty
                  name="age"
                  style={buttonStyle}
                >
                  <MenuItem value={"TP1"}>TP1</MenuItem>
                  <MenuItem value={"TP2"}>TP2</MenuItem>
                  <MenuItem value={"TP3"}>TP3</MenuItem>
                  <MenuItem value={"fortnight"}>fortnight</MenuItem>
                  <MenuItem value={"TP4"}>TP4</MenuItem>
                </Select>
              </FormControl>
            </Grid>
            <Grid item xs={3}>
              <FormControlLabel
                control={
                  <Switch
                    checked={this.state.checkedB}
                    onChange={this.handleDebugChange('checkedB')}
                    value="checkedB"
                    color="primary"
                  />
                }
                label="Debug Mode"
              />
            </Grid>
            <Grid item xs={4}>
              <Button variant="contained" color="primary" onClick={this.kirim} style={buttonStyle}>
                Grade!
            </Button>
            </Grid>
          </Grid>
          <div id="nani" ref="nani" className="aempty">
            {this.state.nyan && <Child />}
            {this.state.showResult && <Result />}
            {this.state.showError && <Errora />}
            {this.state.showCases && <CaseResult />}
          </div>
        </div>
      </MuiThemeProvider>
    );
  }
}

const Errora = () => {
  return (
    <div>
      <div className="result">
        <Typography variant="h4" gutterBottom className="result">
          Connection Error:
              </Typography>
      </div>
      <div>
        <Typography>
          Coba lagi setelah beberapa waktu
          </Typography>
        <Typography>
          Mungkin servernya lagi penuh
          </Typography>
        <Typography>
          btw ini harus di klik terus ya, hanya yang hoki yang bisa grading hiyahiyahiya
          </Typography>
      </div>
    </div>
  )
}


const Result = () => {
  if (isCompileError) {
    return (
      <div>
        <div className="result">
          <Typography variant="h4" gutterBottom className="result">
            Compile Error:
              </Typography>
        </div>
        <div>
          <Typography>
            <pre>
              {compileErrorMessage}
            </pre>
          </Typography>
        </div>
      </div>
    )
  } else {
    return (
      <div>
        <div className="result">
          <Typography variant="h4" gutterBottom className="result">
            Success: {correctAns}/{totalProb}
          </Typography>
        </div>
        <div>
          {listItems}
        </div>
      </div>
    )
  }
}

const CaseResult = () => {
  return (
    <div>
      <div className="result">
        <Typography variant="h4" gutterBottom className="result">
          Test Cases:
          </Typography>
      </div>
      <div>
        {listItems}
      </div>
    </div>
  )
}



const Child = () => (
  <div>
    <div>
      <img src="https://i.imgur.com/Spi2hgG.gif" className="nyan" alt="nyancat" />
    </div>
    <div>
      <Typography variant="h4" gutterBottom>
        {nyanMessage}
      </Typography>
    </div>
  </div>
)
export default App;
